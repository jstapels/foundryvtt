<p>In Foundry Virtual Tabletop, a game session has one <strong>host</strong> and several <strong>clients</strong>. In order to enjoy a multi-player gaming experience the clients must be able to connect to the host which is running the tabletop software.</p>
<hr />

<h2>Hosting Modes</h2>
<p>There are two main hosting modes. The following section attempts to help you understand which hosting mode may be the best choice for you.</p>

<h3>Self-Hosted</h3>
<p>In a self-hosted setting, the application runs on your own computer and creates a local web server which allows other players to connect directly to your computer. The self-hosted mode is most similar to the setup of Fantasy Grounds, Maptool, or GM Forge.</p>

<h4>Advantages</h4>
<ul>
	<li>Free (assuming you have a computer).</li>
	<li>Almost zero setup.</li>
	<li>Easy management of content using your own computer's file system.</li>
</ul>

<h4>Disadvantages</h4>
<ul>
	<li>Your world is only available when the host is running a session.</li>
	<li>Requires clients to be able to connect to your computer - either through a local network or using port forwarding.</li>
	<li>Player experience depends on the host's internet speed.</li>
</ul>

<h3>Dedicated</h3>
<p>In a dedicated hosting setting, the application runs on a persistent web server using node.js and all players (including the GM) connect to the game session. The dedicated hosting mode is most similar to the experience of Roll20 or Astral Tabletop.</p>

<h4>Advantages</h4>
<ul>
	<li>Your world is always online, although gameplay can be paused in between sessions</li>
	<li>Clients connect to your world regardless of local network configurations.</li>
	<li>Network transfer speeds are typically faster than self-hosted resulting in a better player experience.</li>
</ul>

<h4>Disadvantages</h4>
<ul>
	<li>Requires a accessible web server which is configured to serve the Foundry VTT application.</li>
	<li>More complicated initial setup, especially for users who are unfamiliar with web hosting.</li>
	<li>Your world's static files (maps, tokens, music) need to be served from a static location or from the web server.</li>
</ul>
<hr />

<h2>Self-Hosted Configuration</h2>
<p>In a self-hosted configuration, you will need to ensure that clients can connect to your PC using your IP address. There are multiple ways to achieve this and you can use a combination of approaches for different players. By default, Foundry Virtual Tabletop runs on port <strong>30000</strong>.</p>
<p class="note info">For all self-hosted configuration models you will need to be sure that your local operating system firewall is not blocking network traffic for the application. For Windows users, you should be prompted to allow (or deny) a firewall exception when the Foundry VTT application is first started. If you have followed other steps to allow connectivity but users are still unable to connect, be sure to check your Firewall rules.</p>

<h3>Local Area Network</h3>
<p>If your players are on the same network as you, they should be able to connect to your computer using your local IP address. To discover your own local IP address: for Windows check your Connection Settings or use ipconfig from the Command Prompt, for Mac look at Network Settings under System Preferences or use ipconfig in your Terminal, for Linux use hostname -i. Local network players should connect to your local</p>
<p>IP address and port, for example <code>http://x.x.x.x:30000</code>.</p>

<h3>Port Forwarding</h3>
<p>If your players are connecting over the internet, they will use your public IP address. Use a site like http://whatismyip.host/ to easily discover your public IP address. In order for this to work, you will need to forward web traffic for your local network to send the Foundry VTT port to your computer's local IP address. This step is required in order for your network to know where to send the connection.</p>
<p>Port forwarding can be intimidating for some users, but it is the recommended approach as it is more secure than other options. The exact steps to implement port forwarding will depend on your network configuration and hardware. Most frequently, port forwarding is done within your router configuration interface. The website <a href="https://portforward.com/" target="_blank" rel="nofollow noopener">https://portforward.com/</a> has general instructions for the most common router configurations. Once the port is forwarded corretly players can connect to your public IP address in the browser <code>http://x.x.x.x:30000</code>.</p>

<h3>Virtual Private Network</h3>
<p>If your players are remote but port forwarding is not an option, a third option can be to use a VPN service. Please be aware - if you find yourself in this situation, the dedicated hosting option may be a better choice for you. With a virtual private network, other users will have access to details about your computer as well as any content (like documents or pictures) that you are sharing with your local network. If you do choose to go down this route, however, services like Hamachi (<a href="https://www.vpn.net/" target="_blank" rel="nofollow noopener">https://www.vpn.net/</a>) can create a virtual network - once inside a VPN your players can connect to your session using the above instructions for Local Area Network.</p>

<p>For self-hosted installation simply download the zipped application which is suitable for your operating system and extract the archive into a directory of your choice. The first time starting the application under a self-hosted configuration, you may be prompted by your operating system for permission to allow the application to interact with the external network. Be sure to allow this permission otherwise network traffic may be blocked by your operating system firewall.</p>
<hr />

<h2>Dedicated Configuration</h2>
<p>To configure Foundry Virtual Tabletop for a dedicated server configuration there are a few simple steps to follow. Firstly you will need to create a server instance on which you want to host the Foundry VTT application. Secondly you will need to install Node.js and the Foundry VTT software.</p>

<h3>Launch a Server Instance</h3>
<p>The configuration for a dedicated server will vary somewhat depending on your hosting platform and networking requirements. This section provides a simple configuration example for running the server using an AWS instance (<a href="https://aws.amazon.com/ec2/" target="_blank" rel="nofollow noopener">https://aws.amazon.com/ec2/</a>). Foundry Virtual Tabletop can work even with a t2.micro size instance which is supported by the free tier program which is an easy way to begin trying out the software.</p>
<p>To get started, launch a t2.micro (or larger) instance using the Linux distribution of your choice. These instructions are for the standard Amazon Linux AMI. Configure the inbound rules for your instance security group using the AWS dashboard to allow inbound traffic using a <strong>Custom TPC Rule</strong> for port <code>30000</code> (or a different port of your choice). Lastly, connect to your new host via SSH. You will need to configure your SSH client to use the security key-pair provided by AWS.</p>

<h3>Install Software</h3>
<p>To get started with Foundry VTT, you will need to install <a title="Node.js" href="https://nodejs.org" target="_blank" rel="nofollow noopener">Node.js</a> which is used to host the server.</p>
<p class="note info">Note that a relatively modern version of Node.js is required in order to support various security features which are required by the application. Please use Node.js version 12.x or newer.</p>

<h4>For Red Hat / Amazon Linux</h4> <pre><code class="language bash">sudo yum install -y openssl-devel
curl --silent --location https://rpm.nodesource.com/setup_12.x | sudo bash -
sudo yum install -y nodejs
</code></pre>

<h4>For Debian / Ubuntu</h4> <pre><code class="language bash">sudo apt install -y libssl-dev
curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt install -y nodejs
</code></pre>
<p>Once Node.js is installed, next download and extract the latest Foundry Virtual Tabletop Linux version from Patreon. The following commands will create an application directory and a sibling user data directory:</p> <pre><code class="language bash"># Create application and user data directories
cd $HOME
mkdir foundryvtt
mkdir foundrydata

# Install the software
cd foundryvtt
wget &lt;foundry-website-download-url&gt; -O foundryvtt.zip
unzip foundryvtt.zip

# Start running the server
node resources/app/main.js --dataPath=$HOME/foundrydata
</code></pre>

<h3>For MacOS</h3>
<p>Foundry VTT is also supported as a native application on macOS using Electron, however if you wish to host the software using Node.js directly, this is also an option in the MacOS environment. Simply visit <a href="https://nodejs.org/en/download/" target="_blank" rel="nofollow noopener">https://nodejs.org/en/download/</a> and download the macOS installer. Node is installed on your system, you can run the server using the instructions in the below section via your Mac terminal.</p>

<h3>For Windows</h3>
<p>Note that you can run a dedicated server from Windows also, for Windows you should download and install node.js from <a href="https://nodejs.org/en/download/" target="_blank" rel="nofollow noopener">https://nodejs.org/en/download/</a>.</p>
<hr />

<p>For next steps, once the Foundry Virtual Tabletop software is installed see @Article[configuration].</p>