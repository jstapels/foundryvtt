<p>Foundry Virtual Tabletop is a powerful application with lots of features, so it can be overwhelming to a new game-master, even if you have previous experience using virtual tabletop software. This page attempts to walk you through the major concepts in Foundry VTT and guide you through the process of creating your first game-ready Scene.</p>
<p class="note warning">This article is currently a bit of a construction zone, so I apologize for the mess.</p>
<hr />

<h2>Part 1: Installation and Setup</h2>
<p>This page does not go into the installation process in detail. For instructions on how to set up the application and how to configure things for multi-player hosting please see the <a href="../hosting" target="_blank" rel="nofollow noopener">Hosting and Connectivity Guide</a>.</p>

<h3>Worlds, Systems, and Modules</h3>
<p>When first starting Foundry Virtual Tabletop, you will want to install at least one game system upon which to base your worlds. You may also wish to experiment with add-on modules which customize and augment the gameplay experience. It will be helpful to be familiar with the role of each of these concepts.</p>
<dl class="simple"> <dt>Worlds</dt>
	<dd>
		<p>Each world is a collection of data and content which together defines the areas (Scenes), characters (Actors), things (Items), and more that your players will experience during the game. Each world is stored in it&rsquo;s own uniquely named folder inside your user data directory under <code class="docutils literal notranslate">Data/worlds</code>.</p>
	</dd> <dt>Game Systems</dt>
	<dd>
		<p>Each World depends on one (and only one) game System which defines key concepts, data structures, rules, and user interfaces which are specific to the game system being played. Example game systems are <strong>13th Age</strong> or <strong>Pathfinder 2e</strong>. Explore the set of systems which are available for testing on the community wiki page: <a class="reference external" href="https://foundry-vtt-community.github.io/wiki/Community-Game-Systems/" target="_blank" rel="nofollow noopener">https://foundry-vtt-community.github.io/wiki/Community-Game-Systems/</a></p>
	</dd> <dt>Modules</dt>
	<dd>
		<p>Modules extend the core functionality of the Foundry VTT platform by contributing additional features, interfaces, tools, or content. Modules can be designed specifically for one game system, or can be generally applicable to any system. Modules range broadly from quality of life enhancement suites to specialty content packs. Explore the directory of community Modules which are available for testing on the community wiki page: <a class="reference external" href="https://foundry-vtt-community.github.io/wiki/Community-Modules/" target="_blank" rel="nofollow noopener">https://foundry-vtt-community.github.io/wiki/Community-Modules/</a></p>
	</dd>
</dl>

<h3>Creating a World</h3>
<p><img src="../_images/world-config.jpg" alt="../_images/world-config.jpg" /></p>
<p class="caption">This is the World configuration screen, used to create and update worlds.</p>
<p>When creating a new world, you must fill in some mandatory details. The world name is a human-readable string that may contain special characters. The file path to your world defines the subdirectory name within the worlds directory where your data will be stored.</p>
<p><strong>Warning:&nbsp;</strong>The file path for your world will be used in web URLs, so it is best to adhere to common web standards in choosing this name. Avoid spaces or special characters, instead using hyphens to separate multiple terms.</p>
<p>You must choose the Game System that your world will use. The simplest way to install a game system is to use the <strong>Install System</strong> button on the Systems tab of the setup menu. Paste the manifest link for the system into the input field and system installation will occur automatically! If you wish to manually install a system, you can do so by extracting the system directory into <code class="docutils literal notranslate">Data/systems</code>, but it is recommended to use the automated installer.</p>
<p><strong>Warning:&nbsp;</strong>A world&rsquo;s game system cannot be changed once the world is created, since any data created in that world will use the data model and schema for that system.</p>
<p>Lastly, you may optionally provide a text description, background image, or scheduling information for your work to provide it with some additional flavor and help share a brief description for your setting.</p>

<h3>Player Configuration</h3>
<p>One important final setup step is to create and configure the Users who will be joining your game. You should create a User record for each player you expect to join your game. Control of Actors and other permissions are configured at the User level, so having different users allows you to give players access to different content.</p>
<p><img src="../_images/players-config.jpg" alt="../_images/players-config.jpg" /></p>
<p class="caption">This is the Users configuration screen, used to create, edit, or delete User records.</p>
<p>Each user is assigned a permission level which controls what capabilities they will have within the Virtual Tabletop. Additionally each user can have an Access Key which defines a simple password that can be used to restrict access to each User.</p>
<p class="admonition-title">Warning</p>
<p>The Access Keys used in Foundry Virtual Tabletop are NOT cryptographically secure. Please do not reuse any password that you also use for other important accounts or services.</p>
<p>In the above example, I have created three Users for my world: Sam, Jill, and Dan. Each user (including the Game-Master) has a different permission level assigned. Each permission level is described below:</p>
<dl class="simple"> <dt>Gamemaster</dt>
	<dd>
		<p>Users with the Gamemaster permission level have full control over the world and it&rsquo;s data. Be cautious which users you allow to have GM permission, and it is advisable to assign an access key for any GM players.</p>
	</dd> <dt>Player</dt>
	<dd>
		<p>The most basic level of permission. Players can join the game and be given access to control or observe Actors and Items. Basic player accounts do not have some of the permissions which Trusted Players possess.</p>
	</dd> <dt>Trusted Player</dt>
	<dd>
		<p>A player flagged as &ldquo;trusted&rdquo; has some additional capabilities beyond that of an ordinary player. Trusted players can be allowed access to upload image files to the host&rsquo;s file system (token artwork, for example). Trusted players are also allowed to place AoE measurement templates on the game canvas.</p>
	</dd> <dt>Assistant Gamemaster</dt>
	<dd>
		<p>An assistant GM is similar to a regular GameMaster, they can see the entire game canvas and can create or edit all content. Some permissions are limited, however: Assistant GMs are not allowed to delete Actors, Scenes, or Items.</p>
	</dd>
</dl>
<p>Even if you don&rsquo;t have any specific players in mind for your world yet, I recommend creating at least one Player user that you can use for testing out your content from the perspective of a potential player.</p>
<hr />

<h2>Part 2: First Steps in your World</h2>
<p>Once you have created your World you can join the game using your GameMaster user. At this point, you have a blank canvas from which to create brand new content, or import content from modules and compendium packs. There are many possible paths to take in creating a World in Foundry Virtual Tabletop, for the purposes of this tutorial I will walk you through the typical order in which I would set things up, but feel free to explore or find your own path.</p>
<p class="admonition-title">Note</p>
<p>Whenever you load a new World, the game starts as Paused. This prevents players from immediately moving their Tokens. You can pause or un-pause the session at any time by pressing the spacebar.</p>

<h3>Creating a Scene</h3>
<p>My preferred first step in creating a new World is to start by setting up a Scene. Think of Scenes as the locations and maps that your players will explore. Scenes are displayed in the game canvas and contain objects used to populate and define the area like Tokens, Walls, Light Sources, Sounds, Event Triggers, and more.</p>
<p>To create a new Scene, open the Scenes Directory (the map icon, 3rd from the left) in the sidebar on the right-side of the screen. At the bottom of the sidebar, you can create a new Scene directly, or first create a Folder which you can use to organize your Scenes into groups. You can always create Folders later and add existing scenes to them with mouse drag+drop.</p>
<p>To learn all about Scenes and their settings please visit the following page: <a title="The Scene Entity" href="../scenes" target="_blank" rel="nofollow noopener">The Scene Entity</a>.</p>
<p class="admonition-title">Note</p>
<p>For this section of the tutorial, I recommend creating a Scene using a battle-map rather than a regional or world map, as that will make it easier to follow the tutorial. If you want to follow along exactly, you can use map shown in this tutorial which is available on <a href="https://www.deviantart.com/foundryatropos/art/Foundry-Tavern-at-Night-746759206/" target="_blank" rel="nofollow noopener">DeviantArt</a>.</p>
<p><img src="../_images/tutorial-scene.jpg" alt="../_images/tutorial-scene.jpg" /></p>
<p class="caption">I have created an example Scene using the configuration options displayed above.</p>
<p>Once you have created a Scene, let&rsquo;s first learn some basics of navigation. Click and drag with your right-mouse button to pan the scene, shifting the view of the background image. Use your mouse wheel (or touch zoom) to zoom in and out. Once you have a feel for basic Scene navigation, let&rsquo;s learn how to populate the scene with Actors which will represent both player and non-player characters in this area of the world.</p>

<h3>Creating Actors and Placing Tokens</h3>
<p>Once you have created and configured a Scene, a great next step to practice is creating an Actor and placing that Actor within the Scene by creating a Token. To learn all about Actors and their properties please visit the following page: <a title="The Actor Entity" href="../actors" target="_blank" rel="nofollow noopener">The Actor Entity</a>.</p>
<p><img src="../_images/actor-sheet-5e.jpg" alt="../_images/actor-sheet-5e.jpg" /></p>
<p class="caption">Creating an Actor will allow you to test placing Tokens as well as experiencing the Scene from the player&rsquo;s perspective.</p>
<p>Once you have created an Actor, a good idea is to assign permission to that Actor to one of your player User accounts. To do this, right click on the Actor entry in the sidebar and click Permissions. Give the OWNER permission to a user you want to have full control over this Actor.</p>
<p>A typical next step in the Actor creation workflow is to configure the default Token that represents this new Actor. Click the <strong>Configure Token</strong> button at the top of the Actor sheet and follow the instructions on the Actor Entity page linked above.</p>
<p>Once the Actor&rsquo;s default token is configured, click and drag the Actor from the sidebar onto the tabletop canvas for the Scene you created to place the Actor&rsquo;s configured Token into the Scene you created previously.</p>

<h3>Create Walls and Lighting</h3>
<p>TO-DO</p>

<h3>Configure Token Vision and Fog of War</h3>
<p>TO-DO</p>
<hr />

<h2>Part 3: Controlling the Game</h2>
<p>This area of the tutorial is a to-do</p>
<p><strong>Scene Controls:</strong></p>
<ul>
	<li>Preload content</li>
	<li>Set scene as Active</li>
	<li>Pull to Scene</li>
</ul>
<p><strong>Gameplay Controls:</strong></p>
<ul>
	<li>Pausing and unpausing</li>
	<li>Moving and rotating tokens</li>
	<li>Interacting with doors</li>
</ul>
<p><strong>Combat Encounters</strong></p>
<ul>
	<li>Creating combat encounters</li>
	<li>Rolling token initiative</li>
	<li>Adjusting Token health values</li>
</ul>